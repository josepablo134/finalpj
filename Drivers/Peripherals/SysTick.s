	INCLUDE	Peripherals.inc
	INCLUDE	NVIC_hw.inc
	INCLUDE SysTick_hw.inc
	
	AREA	DATA,	READWRITE,	ALIGN=2
DELAY		SPACE		4		;32 bits Variable
SYSTICK		SPACE		4		;32 bits Variable

	AREA    |.text|, CODE, READONLY , ALIGN=2
	THUMB
	EXPORT	SysTick_init
	EXPORT	SysTick_start
	EXPORT	SysTick_stop
	EXPORT	SysTick_read
	EXPORT	SysTick_write
	EXPORT	SysTick_delay
		
SysTick_init
	; 	Limpiar los registros del driver
	LDR		R1,=SYSTICK
	MOV32	R0,#0x00
	STR		R0,[R1]
	;	Limpiar el valor actual del SysTick
	LDR		R1,=(NVIC_BASE + SYSTICK_CURRENT)
	MOV32	R0,#0x0000
	STR		R0,[R1]
	;	Precarga del SysTick
	LDR		R1,=(NVIC_BASE + SYSTICK_RELOAD)
	MOV32	R0,#3999						;	USING (PIOSC/4) 1MS TICK
	STR		R0,[R1]
	;	Pre-Configuracion del SysTick
	LDR		R1,=(NVIC_BASE + SYSTICK_CTL)
	MOV		R0,#( BIT1 ) ;INT ENABLE
	STR		R0,[R1]
	
	;	Activacion de Interrupciones
	LDR		R1,=(NVIC_BASE + NVIC_EN)
	LDR		R2,[R1]
	MOV32	R0,#BIT15
	ORR		R0,R0,R2
	STR		R0,[R1]
	BX      LR;	RETURN

SysTick_start
	;	Activar el SysTick Sin modificar las demas configuraciones
	LDR		R1,=(NVIC_BASE + SYSTICK_CTL)
	LDR		R2,[R1]
	MOV		R0,#0x0001
	ORR		R0,R0,R2
	STR		R0,[R1]
	BX      LR;	RETURN

SysTick_stop
	;	Desactivar el SysTick
	LDR		R1,=(NVIC_BASE + SYSTICK_CTL)
	LDR		R2,[R1]
	MOV32	R0,#( BIT16 + BIT2 + BIT1 )
	AND		R0,R0,R2
	STR		R0,[R1]
	BX      LR;	RETURN

SysTick_read
	;	Retornar el Current value
	LDR		R1,=(NVIC_BASE + SYSTICK_CURRENT)
	LDR		R0,[R1]
	BX      LR;	RETURN

SysTick_write
	;	Escribir el argumento en el RELOAD
	LDR		R1,=(NVIC_BASE + SYSTICK_RELOAD)
	STR		R0,[R1]
	BX      LR;	RETURN
	
SysTick_delay
	LDR		R1,=DELAY
	STR		R0,[R1]
WAITING
;	ESPERAR A QUE DELAY LLEGUE A CERO
	LDR		R0,[R1]
	ANDS	R0,#0xFFFFFFFF	;COMPARE, BRANCH IF ZERO
	BNE		WAITING
	BX		LR;	RETURN
	

; SysTick Interrupt Handler
	EXPORT	SysTick_Handler
SysTick_Handler
	; Incrementar el SYSTICK
	LDR		R1,=SYSTICK
	LDR		R0,[R1]
	ADD		R0,R0,#0x01
	STR		R0,[R1]
	; SI DELAY TIENE ALGO, DECREMENTARLO
	LDR		R1,=DELAY
	LDR		R0,[R1]
	CBZ		R0,SysTick_Handle_exit
	SUB		R0,R0,#0x0001
	STR		R0,[R1]
SysTick_Handle_exit
	BX		LR;Retornar
	ALIGN
	END