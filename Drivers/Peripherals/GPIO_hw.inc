;;//*****************************************************************************
;;//
;;// READ WRITE BIT MASK
;;//
;;//*****************************************************************************
GPIO_O_BIT0				EQU		0x00000100
GPIO_O_BIT1				EQU		0x00000200
GPIO_O_BIT2				EQU		0x00000400
GPIO_O_BIT3				EQU		0x00000800
GPIO_O_BIT4				EQU		0x00001000
GPIO_O_BIT5				EQU		0x00002000
GPIO_O_BIT6				EQU		0x00004000
GPIO_O_BIT7				EQU		0x00008000

;;//*****************************************************************************
;;//
;;// The following are defines for the GPIO register offsets.
;;//
;;//*****************************************************************************

GPIO_O_DATA             EQU		0x00000000  ;// GPIO Data
GPIO_O_DIR              EQU		0x00000400  ;// GPIO Direction
GPIO_O_IS               EQU		0x00000404  ;// GPIO Interrupt Sense
GPIO_O_IBE              EQU		0x00000408  ;// GPIO Interrupt Both Edges
GPIO_O_IEV              EQU		0x0000040C  ;// GPIO Interrupt Event
GPIO_O_IM               EQU		0x00000410  ;// GPIO Interrupt Mask
GPIO_O_RIS              EQU		0x00000414  ;// GPIO Raw Interrupt Status
GPIO_O_MIS              EQU		0x00000418  ;// GPIO Masked Interrupt Status
GPIO_O_ICR              EQU		0x0000041C  ;// GPIO Interrupt Clear
GPIO_O_AFSEL            EQU		0x00000420  ;// GPIO Alternate Function Select
GPIO_O_DR2R             EQU		0x00000500  ;// GPIO 2-mA Drive Select
GPIO_O_DR4R             EQU		0x00000504  ;// GPIO 4-mA Drive Select
GPIO_O_DR8R             EQU		0x00000508  ;// GPIO 8-mA Drive Select
GPIO_O_ODR              EQU		0x0000050C  ;// GPIO Open Drain Select
GPIO_O_PUR              EQU		0x00000510  ;// GPIO Pull-Up Select
GPIO_O_PDR              EQU		0x00000514  ;// GPIO Pull-Down Select
GPIO_O_SLR              EQU		0x00000518  ;// GPIO Slew Rate Control Select
GPIO_O_DEN              EQU		0x0000051C  ;// GPIO Digital Enable
GPIO_O_LOCK             EQU		0x00000520  ;// GPIO Lock
GPIO_O_CR               EQU		0x00000524  ;// GPIO Commit
GPIO_O_AMSEL            EQU		0x00000528  ;// GPIO Analog Mode Select
GPIO_O_PCTL             EQU		0x0000052C  ;// GPIO Port Control
GPIO_O_ADCCTL           EQU		0x00000530  ;// GPIO ADC Control
GPIO_O_DMACTL           EQU		0x00000534  ;// GPIO DMA Control
GPIO_O_SI               EQU		0x00000538  ;// GPIO Select Interrupt
GPIO_O_DR12R            EQU		0x0000053C  ;// GPIO 12-mA Drive Select
GPIO_O_WAKEPEN          EQU		0x00000540  ;// GPIO Wake Pin Enable
GPIO_O_WAKELVL          EQU		0x00000544  ;// GPIO Wake Level
GPIO_O_WAKESTAT         EQU		0x00000548  ;// GPIO Wake Status
GPIO_O_PP               EQU		0x00000FC0  ;// GPIO Peripheral Property
GPIO_O_PC               EQU		0x00000FC4  ;// GPIO Peripheral Configuration

;;//*****************************************************************************
;;//
;;// The following are defines for the bit fields in the GPIO_O_LOCK register.
;;//
;;//*****************************************************************************
GPIO_LOCK_M             EQU		0xFFFFFFFF  ;// GPIO Lock
GPIO_LOCK_UNLOCKED      EQU		0x00000000  ;// The GPIOCR register is unlocked
                                            ;// and may be modified
GPIO_LOCK_LOCKED        EQU		0x00000001  ;// The GPIOCR register is locked
                                            ;// and may not be modified
GPIO_LOCK_KEY           EQU		0x4C4F434B  ;// Unlocks the GPIO_CR register

	END