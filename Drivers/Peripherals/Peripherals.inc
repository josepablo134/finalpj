;
; 	CARGAR 0x20		A LA DIRECCION OPER1
;	CARGAR 0X30 	A LA DIRECCION OPER2
;	CARGAR LA SUMA EN LA DIRECCION RESUL
;

; 	MOV		R0,#0x20
;	LDR		R1,=OPER1
;	STRB	R0,[R1]
;	MOV		R1,#0x30
;	LDR		R4,=OPER2
;	STRB	R2,[R4]
;	
;	UADD8	R3, R0, R2
;	LDR		R1,=RESUL
;	STRB	R3,[R1]

;    /*!
;     *
;     *		MASCARA DE BITS PARA LA MANIPULACION DE REGISTROS Y VARIABLES
;     *
;    */
BIT0				EQU		0x00000001
BIT1				EQU		0x00000002
BIT2				EQU		0x00000004
BIT3				EQU		0x00000008
BIT4				EQU		0x00000010
BIT5				EQU		0x00000020
BIT6				EQU		0x00000040
BIT7				EQU		0x00000080
BIT8				EQU		0x00000100
BIT9				EQU		0x00000200
BIT10				EQU		0x00000400
BIT11				EQU		0x00000800
BIT12				EQU		0x00001000
BIT13				EQU		0x00002000
BIT14				EQU		0x00004000
BIT15				EQU		0x00008000
BIT16				EQU		0x00010000
BIT17				EQU		0x00020000
BIT18				EQU		0x00040000
BIT19				EQU		0x00080000
BIT20				EQU		0x00100000
BIT21				EQU		0x00200000
BIT22				EQU		0x00400000
BIT23				EQU		0x00800000
BIT24				EQU		0x01000000
BIT25				EQU		0x02000000
BIT26				EQU		0x04000000
BIT27				EQU		0x08000000
BIT28				EQU		0x10000000
BIT29				EQU		0x20000000
BIT30				EQU		0x40000000
BIT31				EQU		0x80000000

;/*!
; *	DIRECCIONES DE LOS PERIFERICOS
;*/

;/*!
; *	DIRECCIONES DE LOS PERIFERICOS
;*/
FLASH_BASE              EQU		0x00000000  ; FLASH memory
SRAM_BASE               EQU		0x20000000  ; SRAM memory
WATCHDOG0_BASE          EQU		0x40000000  ; Watchdog0
WATCHDOG1_BASE          EQU		0x40001000  ; Watchdog1
GPIO_PORTA_BASE         EQU		0x40004000  ; GPIO Port A
GPIO_PORTB_BASE         EQU		0x40005000  ; GPIO Port B
GPIO_PORTC_BASE         EQU		0x40006000  ; GPIO Port C
GPIO_PORTD_BASE         EQU		0x40007000  ; GPIO Port D
SSI0_BASE               EQU		0x40008000  ; SSI0
SSI1_BASE               EQU		0x40009000  ; SSI1
SSI2_BASE               EQU		0x4000A000  ; SSI2
SSI3_BASE               EQU		0x4000B000  ; SSI3
UART0_BASE              EQU		0x4000C000  ; UART0
UART1_BASE              EQU		0x4000D000  ; UART1
UART2_BASE              EQU		0x4000E000  ; UART2
UART3_BASE              EQU		0x4000F000  ; UART3
UART4_BASE              EQU		0x40010000  ; UART4
UART5_BASE              EQU		0x40011000  ; UART5
UART6_BASE              EQU		0x40012000  ; UART6
UART7_BASE              EQU		0x40013000  ; UART7
I2C0_BASE               EQU		0x40020000  ; I2C0
I2C1_BASE               EQU		0x40021000  ; I2C1
I2C2_BASE               EQU		0x40022000  ; I2C2
I2C3_BASE               EQU		0x40023000  ; I2C3
GPIO_PORTE_BASE         EQU		0x40024000  ; GPIO Port E
GPIO_PORTF_BASE         EQU		0x40025000  ; GPIO Port F
GPIO_PORTG_BASE         EQU		0x40026000  ; GPIO Port G
GPIO_PORTH_BASE         EQU		0x40027000  ; GPIO Port H
PWM0_BASE               EQU		0x40028000  ; Pulse Width Modulator (PWM)
PWM1_BASE               EQU		0x40029000  ; Pulse Width Modulator (PWM)
QEI0_BASE               EQU		0x4002C000  ; QEI0
QEI1_BASE               EQU		0x4002D000  ; QEI1
TIMER0_BASE             EQU		0x40030000  ; Timer0
TIMER1_BASE             EQU		0x40031000  ; Timer1
TIMER2_BASE             EQU		0x40032000  ; Timer2
TIMER3_BASE             EQU		0x40033000  ; Timer3
TIMER4_BASE             EQU		0x40034000  ; Timer4
TIMER5_BASE             EQU		0x40035000  ; Timer5
WTIMER0_BASE            EQU		0x40036000  ; Wide Timer0
WTIMER1_BASE            EQU		0x40037000  ; Wide Timer1
ADC0_BASE               EQU		0x40038000  ; ADC0
ADC1_BASE               EQU		0x40039000  ; ADC1
COMP_BASE               EQU		0x4003C000  ; Analog comparators
GPIO_PORTJ_BASE         EQU		0x4003D000  ; GPIO Port J
CAN0_BASE               EQU		0x40040000  ; CAN0
CAN1_BASE               EQU		0x40041000  ; CAN1
WTIMER2_BASE            EQU		0x4004C000  ; Wide Timer2
WTIMER3_BASE            EQU		0x4004D000  ; Wide Timer3
WTIMER4_BASE            EQU		0x4004E000  ; Wide Timer4
WTIMER5_BASE            EQU		0x4004F000  ; Wide Timer5
USB0_BASE               EQU		0x40050000  ; USB 0 Controller
GPIO_PORTA_AHB_BASE     EQU		0x40058000  ; GPIO Port A (high speed)
GPIO_PORTB_AHB_BASE     EQU		0x40059000  ; GPIO Port B (high speed)
GPIO_PORTC_AHB_BASE     EQU		0x4005A000  ; GPIO Port C (high speed)
GPIO_PORTD_AHB_BASE     EQU		0x4005B000  ; GPIO Port D (high speed)
GPIO_PORTE_AHB_BASE     EQU		0x4005C000  ; GPIO Port E (high speed)
GPIO_PORTF_AHB_BASE     EQU		0x4005D000  ; GPIO Port F (high speed)
GPIO_PORTG_AHB_BASE     EQU		0x4005E000  ; GPIO Port G (high speed)
GPIO_PORTH_AHB_BASE     EQU		0x4005F000  ; GPIO Port H (high speed)
GPIO_PORTJ_AHB_BASE     EQU		0x40060000  ; GPIO Port J (high speed)
GPIO_PORTK_BASE         EQU		0x40061000  ; GPIO Port K
GPIO_PORTL_BASE         EQU		0x40062000  ; GPIO Port L
GPIO_PORTM_BASE         EQU		0x40063000  ; GPIO Port M
GPIO_PORTN_BASE         EQU		0x40064000  ; GPIO Port N
GPIO_PORTP_BASE         EQU		0x40065000  ; GPIO Port P
GPIO_PORTQ_BASE         EQU		0x40066000  ; GPIO Port Q
GPIO_PORTR_BASE         EQU		0x40067000  ; General-Purpose Input/Outputs (GPIOs)
GPIO_PORTS_BASE         EQU		0x40068000  ; General-Purpose Input/Outputs (GPIOs)
GPIO_PORTT_BASE         EQU		0x40069000  ; General-Purpose Input/Outputs (GPIOs)
EEPROM_BASE             EQU		0x400AF000  ; EEPROM memory
ONEWIRE0_BASE           EQU		0x400B6000  ; 1-Wire Master Module
I2C8_BASE               EQU		0x400B8000  ; I2C8
I2C9_BASE               EQU		0x400B9000  ; I2C9
I2C4_BASE               EQU		0x400C0000  ; I2C4
I2C5_BASE               EQU		0x400C1000  ; I2C5
I2C6_BASE               EQU		0x400C2000  ; I2C6
I2C7_BASE               EQU		0x400C3000  ; I2C7
EPI0_BASE               EQU		0x400D0000  ; EPI0
TIMER6_BASE             EQU		0x400E0000  ; General-Purpose Timers
TIMER7_BASE             EQU		0x400E1000  ; General-Purpose Timers
EMAC0_BASE              EQU		0x400EC000  ; Ethernet Controller
SYSEXC_BASE             EQU		0x400F9000  ; System Exception Module
HIB_BASE                EQU		0x400FC000  ; Hibernation Module
FLASH_CTRL_BASE         EQU		0x400FD000  ; FLASH Controller
SYSCTL_BASE             EQU		0x400FE000  ; System Control
UDMA_BASE               EQU		0x400FF000  ; uDMA Controller
CCM0_BASE               EQU		0x44030000  ; Cyclical Redundancy Check (CRC)
SHAMD5_BASE             EQU		0x44034000  ; SHA/MD5 Accelerator
AES_BASE                EQU		0x44036000  ; Advance Encryption
                                            ; Hardware-Accelerated Module
DES_BASE                EQU		0x44038000  ; Data Encryption Standard
                                            ; Accelerator (DES)
LCD0_BASE               EQU		0x44050000  ; LCD Controller
ITM_BASE                EQU		0xE0000000  ; Instrumentation Trace Macrocell
DWT_BASE                EQU		0xE0001000  ; Data Watchpoint and Trace
FPB_BASE                EQU		0xE0002000  ; FLASH Patch and Breakpoint
NVIC_BASE               EQU		0xE000E000  ; Nested Vectored Interrupt Ctrl
TPIU_BASE               EQU		0xE0040000  ; Trace Port Interface Unit

	END