	; void		GPIO_init(uint32_t	BASE );
	EXTERN	GPIO_init
	
	; uint8_t	GPIO_read(uint32_t	BASE , uint8_t MASK);
	EXTERN	GPIO_read
	
	; void	GPIO_write(uint32_t	BASE , uint8_t MASK , uint8_t DATA);
	EXTERN	GPIO_write
	
	; void	GPIO_xor(uint32_t	BASE , uint8_t MASK , uint8_t DATA);
	EXTERN	GPIO_xor
	
	; void	GPIO_and(uint32_t	BASE , uint8_t MASK , uint8_t DATA);
	EXTERN	GPIO_and
	
	; void	GPIO_or(uint32_t	BASE , uint8_t MASK , uint8_t DATA);
	EXTERN	PGPIO_or
	END
