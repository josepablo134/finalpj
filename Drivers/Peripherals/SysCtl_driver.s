	AREA    |.text|, CODE, READONLY
	THUMB
SYSCTL_RCGCBASE			EQU		0x400fe600
		
	EXPORT	SysCtl_enable
	EXPORT	SysCtl_disable
	EXPORT	SysCtl_ready

;void SysCtl_enablePERIPH(uint32_t	PERIPHERAL)
SysCtl_enable
	; Verificar la identidad de R0
	; Crear una mascara de bits
	; Utilizar la mascara para encender el periferico
	;	HWREGBITW(SYSCTL_RCGCBASE + ((ui32Peripheral & 0xff00) >> 8), ui32Peripheral & 0xff) = 1;
	;	HWREG(((uint32_t)(x) & 0xF2000000) |  (((uint32_t)(x) & 0x000FFFFF) << 5) | ((b) << 2))
	AND		R1,R0,#0xFF00
	LSR		R1,R1,#0x08
	LDR		R2,=SYSCTL_RCGCBASE
	ADD		R1,R1,R2		;R1 = x
	AND		R0,R0,#0xFF		;R0 = b
	
	LSL		R0,R0,#0x02
	LDR		R2,=(0xFFFFF)
	AND		R2,R1,R2
	LSL		R2,R2,#0x05
	ORR		R0,R2,R0		;R0 = (((uint32_t)(x) & 0x000FFFFF) << 5) | ((b) << 2))
	
	LDR		R2,=(0xF2000000)
	AND		R1,R1,R2		;R1 = ((uint32_t)(x) & 0xF2000000)
	
	ORR		R0,R0,R1
	MOV		R1,#0x01
	STR		R1,[R0]
	
	BX	LR
SysCtl_disable
	; Verificar la identidad de R0
	; Crear una mascara de bits
	; Utilizar la mascara para apagar el periferico
	BX	LR
SysCtl_ready
	; Verificar la identidad de R0
	; Crear una mascara de bits
	; Utilizar la mascara para leer el estado del periferico con una and
	;    return(HWREGBITW(SYSCTL_PRBASE + ((ui32Peripheral & 0xff00) >> 8), ui32Peripheral & 0xff));
	BX	LR

	ALIGN
	END
