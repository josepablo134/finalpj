;//*****************************************************************************
;//
;// The following are defines for the System Control register addresses.
;//
;//*****************************************************************************
SYSCTL_DID0             EQU 0x400FE000  ;// Device Identification 0
SYSCTL_DID1             EQU 0x400FE004  ;// Device Identification 1
SYSCTL_DC0              EQU 0x400FE008  ;// Device Capabilities 0
SYSCTL_DC1              EQU 0x400FE010  ;// Device Capabilities 1
SYSCTL_DC2              EQU 0x400FE014  ;// Device Capabilities 2
SYSCTL_DC3              EQU 0x400FE018  ;// Device Capabilities 3
SYSCTL_DC4              EQU 0x400FE01C  ;// Device Capabilities 4
SYSCTL_DC5              EQU 0x400FE020  ;// Device Capabilities 5
SYSCTL_DC6              EQU 0x400FE024  ;// Device Capabilities 6
SYSCTL_DC7              EQU 0x400FE028  ;// Device Capabilities 7
SYSCTL_DC8              EQU 0x400FE02C  ;// Device Capabilities 8
SYSCTL_PBORCTL          EQU 0x400FE030  ;// Brown-Out Reset Control
SYSCTL_PTBOCTL          EQU 0x400FE038  ;// Power-Temp Brown Out Control
SYSCTL_SRCR0            EQU 0x400FE040  ;// Software Reset Control 0
SYSCTL_SRCR1            EQU 0x400FE044  ;// Software Reset Control 1
SYSCTL_SRCR2            EQU 0x400FE048  ;// Software Reset Control 2
SYSCTL_RIS              EQU 0x400FE050  ;// Raw Interrupt Status
SYSCTL_IMC              EQU 0x400FE054  ;// Interrupt Mask Control
SYSCTL_MISC             EQU 0x400FE058  ;// Masked Interrupt Status and
                                            ;// Clear
SYSCTL_RESC             EQU 0x400FE05C  ;// Reset Cause
SYSCTL_PWRTC            EQU 0x400FE060  ;// Power-Temperature Cause
SYSCTL_RCC              EQU 0x400FE060  ;// Run-Mode Clock Configuration
SYSCTL_NMIC             EQU 0x400FE064  ;// NMI Cause Register
SYSCTL_GPIOHBCTL        EQU 0x400FE06C  ;// GPIO High-Performance Bus
                                            ;// Control
SYSCTL_RCC2             EQU 0x400FE070  ;// Run-Mode Clock Configuration 2
SYSCTL_MOSCCTL          EQU 0x400FE07C  ;// Main Oscillator Control
SYSCTL_RSCLKCFG         EQU 0x400FE0B0  ;// Run and Sleep Mode Configuration
                                            ;// Register
SYSCTL_MEMTIM0          EQU 0x400FE0C0  ;// Memory Timing Parameter Register
                                            ;// 0 for Main Flash and EEPROM
SYSCTL_RCGC0            EQU 0x400FE100  ;// Run Mode Clock Gating Control
                                            ;// Register 0
SYSCTL_RCGC1            EQU 0x400FE104  ;// Run Mode Clock Gating Control
                                            ;// Register 1
SYSCTL_RCGC2            EQU 0x400FE108  ;// Run Mode Clock Gating Control
                                            ;// Register 2
SYSCTL_SCGC0            EQU 0x400FE110  ;// Sleep Mode Clock Gating Control
                                            ;// Register 0
SYSCTL_SCGC1            EQU 0x400FE114  ;// Sleep Mode Clock Gating Control
                                            ;// Register 1
SYSCTL_SCGC2            EQU 0x400FE118  ;// Sleep Mode Clock Gating Control
                                            ;// Register 2
SYSCTL_DCGC0            EQU 0x400FE120  ;// Deep Sleep Mode Clock Gating
                                            ;// Control Register 0
SYSCTL_DCGC1            EQU 0x400FE124  ;// Deep-Sleep Mode Clock Gating
                                            ;// Control Register 1
SYSCTL_DCGC2            EQU 0x400FE128  ;// Deep Sleep Mode Clock Gating
                                            ;// Control Register 2
SYSCTL_ALTCLKCFG        EQU 0x400FE138  ;// Alternate Clock Configuration
SYSCTL_DSLPCLKCFG       EQU 0x400FE144  ;// Deep Sleep Clock Configuration
SYSCTL_DSCLKCFG         EQU 0x400FE144  ;// Deep Sleep Clock Configuration
                                            ;// Register
SYSCTL_DIVSCLK          EQU 0x400FE148  ;// Divisor and Source Clock
                                            ;// Configuration
SYSCTL_SYSPROP          EQU 0x400FE14C  ;// System Properties
SYSCTL_PIOSCCAL         EQU 0x400FE150  ;// Precision Internal Oscillator
                                            ;// Calibration
SYSCTL_PIOSCSTAT        EQU 0x400FE154  ;// Precision Internal Oscillator
                                            ;// Statistics
SYSCTL_PLLFREQ0         EQU 0x400FE160  ;// PLL Frequency 0
SYSCTL_PLLFREQ1         EQU 0x400FE164  ;// PLL Frequency 1
SYSCTL_PLLSTAT          EQU 0x400FE168  ;// PLL Status
SYSCTL_SLPPWRCFG        EQU 0x400FE188  ;// Sleep Power Configuration
SYSCTL_DSLPPWRCFG       EQU 0x400FE18C  ;// Deep-Sleep Power Configuration
SYSCTL_DC9              EQU 0x400FE190  ;// Device Capabilities 9
SYSCTL_NVMSTAT          EQU 0x400FE1A0  ;// Non-Volatile Memory Information
SYSCTL_LDOSPCTL         EQU 0x400FE1B4  ;// LDO Sleep Power Control
SYSCTL_LDODPCTL         EQU 0x400FE1BC  ;// LDO Deep-Sleep Power Control
SYSCTL_RESBEHAVCTL      EQU 0x400FE1D8  ;// Reset Behavior Control Register
SYSCTL_HSSR             EQU 0x400FE1F4  ;// Hardware System Service Request
SYSCTL_USBPDS           EQU 0x400FE280  ;// USB Power Domain Status
SYSCTL_USBMPC           EQU 0x400FE284  ;// USB Memory Power Control
SYSCTL_EMACPDS          EQU 0x400FE288  ;// Ethernet MAC Power Domain Status
SYSCTL_EMACMPC          EQU 0x400FE28C  ;// Ethernet MAC Memory Power
                                            ;// Control
SYSCTL_LCDMPC           EQU 0x400FE294  ;// LCD Memory Power Control
SYSCTL_PPWD             EQU 0x400FE300  ;// Watchdog Timer Peripheral
                                            ;// Present
SYSCTL_PPTIMER          EQU 0x400FE304  ;// 16/32-Bit General-Purpose Timer
                                            ;// Peripheral Present
SYSCTL_PPGPIO           EQU 0x400FE308  ;// General-Purpose Input/Output
                                            ;// Peripheral Present
SYSCTL_PPDMA            EQU 0x400FE30C  ;// Micro Direct Memory Access
                                            ;// Peripheral Present
SYSCTL_PPEPI            EQU 0x400FE310  ;// EPI Peripheral Present
SYSCTL_PPHIB            EQU 0x400FE314  ;// Hibernation Peripheral Present
SYSCTL_PPUART           EQU 0x400FE318  ;// Universal Asynchronous
                                            ;// Receiver/Transmitter Peripheral
                                            ;// Present
SYSCTL_PPSSI            EQU 0x400FE31C  ;// Synchronous Serial Interface
                                            ;// Peripheral Present
SYSCTL_PPI2C            EQU 0x400FE320  ;// Inter-Integrated Circuit
                                            ;// Peripheral Present
SYSCTL_PPUSB            EQU 0x400FE328  ;// Universal Serial Bus Peripheral
                                            ;// Present
SYSCTL_PPEPHY           EQU 0x400FE330  ;// Ethernet PHY Peripheral Present
SYSCTL_PPCAN            EQU 0x400FE334  ;// Controller Area Network
                                            ;// Peripheral Present
SYSCTL_PPADC            EQU 0x400FE338  ;// Analog-to-Digital Converter
                                            ;// Peripheral Present
SYSCTL_PPACMP           EQU 0x400FE33C  ;// Analog Comparator Peripheral
                                            ;// Present
SYSCTL_PPPWM            EQU 0x400FE340  ;// Pulse Width Modulator Peripheral
                                            ;// Present
SYSCTL_PPQEI            EQU 0x400FE344  ;// Quadrature Encoder Interface
                                            ;// Peripheral Present
SYSCTL_PPLPC            EQU 0x400FE348  ;// Low Pin Count Interface
                                            ;// Peripheral Present
SYSCTL_PPPECI           EQU 0x400FE350  ;// Platform Environment Control
                                            ;// Interface Peripheral Present
SYSCTL_PPFAN            EQU 0x400FE354  ;// Fan Control Peripheral Present
SYSCTL_PPEEPROM         EQU 0x400FE358  ;// EEPROM Peripheral Present
SYSCTL_PPWTIMER         EQU 0x400FE35C  ;// 32/64-Bit Wide General-Purpose
                                            ;// Timer Peripheral Present
SYSCTL_PPRTS            EQU 0x400FE370  ;// Remote Temperature Sensor
                                            ;// Peripheral Present
SYSCTL_PPCCM            EQU 0x400FE374  ;// CRC and Cryptographic Modules
                                            ;// Peripheral Present
SYSCTL_PPLCD            EQU 0x400FE390  ;// LCD Peripheral Present
SYSCTL_PPOWIRE          EQU 0x400FE398  ;// 1-Wire Peripheral Present
SYSCTL_PPEMAC           EQU 0x400FE39C  ;// Ethernet MAC Peripheral Present
SYSCTL_PPHIM            EQU 0x400FE3A4  ;// Human Interface Master
                                            ;// Peripheral Present
SYSCTL_SRWD             EQU 0x400FE500  ;// Watchdog Timer Software Reset
SYSCTL_SRTIMER          EQU 0x400FE504  ;// 16/32-Bit General-Purpose Timer
                                            ;// Software Reset
SYSCTL_SRGPIO           EQU 0x400FE508  ;// General-Purpose Input/Output
                                            ;// Software Reset
SYSCTL_SRDMA            EQU 0x400FE50C  ;// Micro Direct Memory Access
                                            ;// Software Reset
SYSCTL_SREPI            EQU 0x400FE510  ;// EPI Software Reset
SYSCTL_SRHIB            EQU 0x400FE514  ;// Hibernation Software Reset
SYSCTL_SRUART           EQU 0x400FE518  ;// Universal Asynchronous
                                            ;// Receiver/Transmitter Software
                                            ;// Reset
SYSCTL_SRSSI            EQU 0x400FE51C  ;// Synchronous Serial Interface
                                            ;// Software Reset
SYSCTL_SRI2C            EQU 0x400FE520  ;// Inter-Integrated Circuit
                                            ;// Software Reset
SYSCTL_SRUSB            EQU 0x400FE528  ;// Universal Serial Bus Software
                                            ;// Reset
SYSCTL_SREPHY           EQU 0x400FE530  ;// Ethernet PHY Software Reset
SYSCTL_SRCAN            EQU 0x400FE534  ;// Controller Area Network Software
                                            ;// Reset
SYSCTL_SRADC            EQU 0x400FE538  ;// Analog-to-Digital Converter
                                            ;// Software Reset
SYSCTL_SRACMP           EQU 0x400FE53C  ;// Analog Comparator Software Reset
SYSCTL_SRPWM            EQU 0x400FE540  ;// Pulse Width Modulator Software
                                            ;// Reset
SYSCTL_SRQEI            EQU 0x400FE544  ;// Quadrature Encoder Interface
                                            ;// Software Reset
SYSCTL_SREEPROM         EQU 0x400FE558  ;// EEPROM Software Reset
SYSCTL_SRWTIMER         EQU 0x400FE55C  ;// 32/64-Bit Wide General-Purpose
                                            ;// Timer Software Reset
SYSCTL_SRCCM            EQU 0x400FE574  ;// CRC and Cryptographic Modules
                                            ;// Software Reset
SYSCTL_SRLCD            EQU 0x400FE590  ;// LCD Controller Software Reset
SYSCTL_SROWIRE          EQU 0x400FE598  ;// 1-Wire Software Reset
SYSCTL_SREMAC           EQU 0x400FE59C  ;// Ethernet MAC Software Reset
SYSCTL_RCGCWD           EQU 0x400FE600  ;// Watchdog Timer Run Mode Clock
                                            ;// Gating Control
SYSCTL_RCGCTIMER        EQU 0x400FE604  ;// 16/32-Bit General-Purpose Timer
                                            ;// Run Mode Clock Gating Control
SYSCTL_RCGCGPIO         EQU 0x400FE608  ;// General-Purpose Input/Output Run
                                            ;// Mode Clock Gating Control
SYSCTL_RCGCDMA          EQU 0x400FE60C  ;// Micro Direct Memory Access Run
                                            ;// Mode Clock Gating Control
SYSCTL_RCGCEPI          EQU 0x400FE610  ;// EPI Run Mode Clock Gating
                                            ;// Control
SYSCTL_RCGCHIB          EQU 0x400FE614  ;// Hibernation Run Mode Clock
                                            ;// Gating Control
SYSCTL_RCGCUART         EQU 0x400FE618  ;// Universal Asynchronous
                                            ;// Receiver/Transmitter Run Mode
                                            ;// Clock Gating Control
SYSCTL_RCGCSSI          EQU 0x400FE61C  ;// Synchronous Serial Interface Run
                                            ;// Mode Clock Gating Control
SYSCTL_RCGCI2C          EQU 0x400FE620  ;// Inter-Integrated Circuit Run
                                            ;// Mode Clock Gating Control
SYSCTL_RCGCUSB          EQU 0x400FE628  ;// Universal Serial Bus Run Mode
                                            ;// Clock Gating Control
SYSCTL_RCGCEPHY         EQU 0x400FE630  ;// Ethernet PHY Run Mode Clock
                                            ;// Gating Control
SYSCTL_RCGCCAN          EQU 0x400FE634  ;// Controller Area Network Run Mode
                                            ;// Clock Gating Control
SYSCTL_RCGCADC          EQU 0x400FE638  ;// Analog-to-Digital Converter Run
                                            ;// Mode Clock Gating Control
SYSCTL_RCGCACMP         EQU 0x400FE63C  ;// Analog Comparator Run Mode Clock
                                            ;// Gating Control
SYSCTL_RCGCPWM          EQU 0x400FE640  ;// Pulse Width Modulator Run Mode
                                            ;// Clock Gating Control
SYSCTL_RCGCQEI          EQU 0x400FE644  ;// Quadrature Encoder Interface Run
                                            ;// Mode Clock Gating Control
SYSCTL_RCGCEEPROM       EQU 0x400FE658  ;// EEPROM Run Mode Clock Gating
                                            ;// Control
SYSCTL_RCGCWTIMER       EQU 0x400FE65C  ;// 32/64-Bit Wide General-Purpose
                                            ;// Timer Run Mode Clock Gating
                                            ;// Control
SYSCTL_RCGCCCM          EQU 0x400FE674  ;// CRC and Cryptographic Modules
                                            ;// Run Mode Clock Gating Control
SYSCTL_RCGCLCD          EQU 0x400FE690  ;// LCD Controller Run Mode Clock
                                            ;// Gating Control
SYSCTL_RCGCOWIRE        EQU 0x400FE698  ;// 1-Wire Run Mode Clock Gating
                                            ;// Control
SYSCTL_RCGCEMAC         EQU 0x400FE69C  ;// Ethernet MAC Run Mode Clock
                                            ;// Gating Control
SYSCTL_SCGCWD           EQU 0x400FE700  ;// Watchdog Timer Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_SCGCTIMER        EQU 0x400FE704  ;// 16/32-Bit General-Purpose Timer
                                            ;// Sleep Mode Clock Gating Control
SYSCTL_SCGCGPIO         EQU 0x400FE708  ;// General-Purpose Input/Output
                                            ;// Sleep Mode Clock Gating Control
SYSCTL_SCGCDMA          EQU 0x400FE70C  ;// Micro Direct Memory Access Sleep
                                            ;// Mode Clock Gating Control
SYSCTL_SCGCEPI          EQU 0x400FE710  ;// EPI Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_SCGCHIB          EQU 0x400FE714  ;// Hibernation Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_SCGCUART         EQU 0x400FE718  ;// Universal Asynchronous
                                            ;// Receiver/Transmitter Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_SCGCSSI          EQU 0x400FE71C  ;// Synchronous Serial Interface
                                            ;// Sleep Mode Clock Gating Control
SYSCTL_SCGCI2C          EQU 0x400FE720  ;// Inter-Integrated Circuit Sleep
                                            ;// Mode Clock Gating Control
SYSCTL_SCGCUSB          EQU 0x400FE728  ;// Universal Serial Bus Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_SCGCEPHY         EQU 0x400FE730  ;// Ethernet PHY Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_SCGCCAN          EQU 0x400FE734  ;// Controller Area Network Sleep
                                            ;// Mode Clock Gating Control
SYSCTL_SCGCADC          EQU 0x400FE738  ;// Analog-to-Digital Converter
                                            ;// Sleep Mode Clock Gating Control
SYSCTL_SCGCACMP         EQU 0x400FE73C  ;// Analog Comparator Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_SCGCPWM          EQU 0x400FE740  ;// Pulse Width Modulator Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_SCGCQEI          EQU 0x400FE744  ;// Quadrature Encoder Interface
                                            ;// Sleep Mode Clock Gating Control
SYSCTL_SCGCEEPROM       EQU 0x400FE758  ;// EEPROM Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_SCGCWTIMER       EQU 0x400FE75C  ;// 32/64-Bit Wide General-Purpose
                                            ;// Timer Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_SCGCCCM          EQU 0x400FE774  ;// CRC and Cryptographic Modules
                                            ;// Sleep Mode Clock Gating Control
SYSCTL_SCGCLCD          EQU 0x400FE790  ;// LCD Controller Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_SCGCOWIRE        EQU 0x400FE798  ;// 1-Wire Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_SCGCEMAC         EQU 0x400FE79C  ;// Ethernet MAC Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_DCGCWD           EQU 0x400FE800  ;// Watchdog Timer Deep-Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_DCGCTIMER        EQU 0x400FE804  ;// 16/32-Bit General-Purpose Timer
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCGPIO         EQU 0x400FE808  ;// General-Purpose Input/Output
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCDMA          EQU 0x400FE80C  ;// Micro Direct Memory Access
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCEPI          EQU 0x400FE810  ;// EPI Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCHIB          EQU 0x400FE814  ;// Hibernation Deep-Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_DCGCUART         EQU 0x400FE818  ;// Universal Asynchronous
                                            ;// Receiver/Transmitter Deep-Sleep
                                            ;// Mode Clock Gating Control
SYSCTL_DCGCSSI          EQU 0x400FE81C  ;// Synchronous Serial Interface
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCI2C          EQU 0x400FE820  ;// Inter-Integrated Circuit
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCUSB          EQU 0x400FE828  ;// Universal Serial Bus Deep-Sleep
                                            ;// Mode Clock Gating Control
SYSCTL_DCGCEPHY         EQU 0x400FE830  ;// Ethernet PHY Deep-Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_DCGCCAN          EQU 0x400FE834  ;// Controller Area Network
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCADC          EQU 0x400FE838  ;// Analog-to-Digital Converter
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCACMP         EQU 0x400FE83C  ;// Analog Comparator Deep-Sleep
                                            ;// Mode Clock Gating Control
SYSCTL_DCGCPWM          EQU 0x400FE840  ;// Pulse Width Modulator Deep-Sleep
                                            ;// Mode Clock Gating Control
SYSCTL_DCGCQEI          EQU 0x400FE844  ;// Quadrature Encoder Interface
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCEEPROM       EQU 0x400FE858  ;// EEPROM Deep-Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_DCGCWTIMER       EQU 0x400FE85C  ;// 32/64-Bit Wide General-Purpose
                                            ;// Timer Deep-Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_DCGCCCM          EQU 0x400FE874  ;// CRC and Cryptographic Modules
                                            ;// Deep-Sleep Mode Clock Gating
                                            ;// Control
SYSCTL_DCGCLCD          EQU 0x400FE890  ;// LCD Controller Deep-Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_DCGCOWIRE        EQU 0x400FE898  ;// 1-Wire Deep-Sleep Mode Clock
                                            ;// Gating Control
SYSCTL_DCGCEMAC         EQU 0x400FE89C  ;// Ethernet MAC Deep-Sleep Mode
                                            ;// Clock Gating Control
SYSCTL_PCWD             EQU 0x400FE900  ;// Watchdog Timer Power Control
SYSCTL_PCTIMER          EQU 0x400FE904  ;// 16/32-Bit General-Purpose Timer
                                            ;// Power Control
SYSCTL_PCGPIO           EQU 0x400FE908  ;// General-Purpose Input/Output
                                            ;// Power Control
SYSCTL_PCDMA            EQU 0x400FE90C  ;// Micro Direct Memory Access Power
                                            ;// Control
SYSCTL_PCEPI            EQU 0x400FE910  ;// External Peripheral Interface
                                            ;// Power Control
SYSCTL_PCHIB            EQU 0x400FE914  ;// Hibernation Power Control
SYSCTL_PCUART           EQU 0x400FE918  ;// Universal Asynchronous
                                            ;// Receiver/Transmitter Power
                                            ;// Control
SYSCTL_PCSSI            EQU 0x400FE91C  ;// Synchronous Serial Interface
                                            ;// Power Control
SYSCTL_PCI2C            EQU 0x400FE920  ;// Inter-Integrated Circuit Power
                                            ;// Control
SYSCTL_PCUSB            EQU 0x400FE928  ;// Universal Serial Bus Power
                                            ;// Control
SYSCTL_PCEPHY           EQU 0x400FE930  ;// Ethernet PHY Power Control
SYSCTL_PCCAN            EQU 0x400FE934  ;// Controller Area Network Power
                                            ;// Control
SYSCTL_PCADC            EQU 0x400FE938  ;// Analog-to-Digital Converter
                                            ;// Power Control
SYSCTL_PCACMP           EQU 0x400FE93C  ;// Analog Comparator Power Control
SYSCTL_PCPWM            EQU 0x400FE940  ;// Pulse Width Modulator Power
                                            ;// Control
SYSCTL_PCQEI            EQU 0x400FE944  ;// Quadrature Encoder Interface
                                            ;// Power Control
SYSCTL_PCEEPROM         EQU 0x400FE958  ;// EEPROM Power Control
SYSCTL_PCCCM            EQU 0x400FE974  ;// CRC and Cryptographic Modules
                                            ;// Power Control
SYSCTL_PCLCD            EQU 0x400FE990  ;// LCD Controller Power Control
SYSCTL_PCOWIRE          EQU 0x400FE998  ;// 1-Wire Power Control
SYSCTL_PCEMAC           EQU 0x400FE99C  ;// Ethernet MAC Power Control
SYSCTL_PRWD             EQU 0x400FEA00  ;// Watchdog Timer Peripheral Ready
SYSCTL_PRTIMER          EQU 0x400FEA04  ;// 16/32-Bit General-Purpose Timer
                                            ;// Peripheral Ready
SYSCTL_PRGPIO           EQU 0x400FEA08  ;// General-Purpose Input/Output
                                            ;// Peripheral Ready
SYSCTL_PRDMA            EQU 0x400FEA0C  ;// Micro Direct Memory Access
                                            ;// Peripheral Ready
SYSCTL_PREPI            EQU 0x400FEA10  ;// EPI Peripheral Ready
SYSCTL_PRHIB            EQU 0x400FEA14  ;// Hibernation Peripheral Ready
SYSCTL_PRUART           EQU 0x400FEA18  ;// Universal Asynchronous
                                            ;// Receiver/Transmitter Peripheral
                                            ;// Ready
SYSCTL_PRSSI            EQU 0x400FEA1C  ;// Synchronous Serial Interface
                                            ;// Peripheral Ready
SYSCTL_PRI2C            EQU 0x400FEA20  ;// Inter-Integrated Circuit
                                            ;// Peripheral Ready
SYSCTL_PRUSB            EQU 0x400FEA28  ;// Universal Serial Bus Peripheral
                                            ;// Ready
SYSCTL_PREPHY           EQU 0x400FEA30  ;// Ethernet PHY Peripheral Ready
SYSCTL_PRCAN            EQU 0x400FEA34  ;// Controller Area Network
                                            ;// Peripheral Ready
SYSCTL_PRADC            EQU 0x400FEA38  ;// Analog-to-Digital Converter
                                            ;// Peripheral Ready
SYSCTL_PRACMP           EQU 0x400FEA3C  ;// Analog Comparator Peripheral
                                            ;// Ready
SYSCTL_PRPWM            EQU 0x400FEA40  ;// Pulse Width Modulator Peripheral
                                            ;// Ready
SYSCTL_PRQEI            EQU 0x400FEA44  ;// Quadrature Encoder Interface
                                            ;// Peripheral Ready
SYSCTL_PREEPROM         EQU 0x400FEA58  ;// EEPROM Peripheral Ready
SYSCTL_PRWTIMER         EQU 0x400FEA5C  ;// 32/64-Bit Wide General-Purpose
                                            ;// Timer Peripheral Ready
SYSCTL_PRCCM            EQU 0x400FEA74  ;// CRC and Cryptographic Modules
                                            ;// Peripheral Ready
SYSCTL_PRLCD            EQU 0x400FEA90  ;// LCD Controller Peripheral Ready
SYSCTL_PROWIRE          EQU 0x400FEA98  ;// 1-Wire Peripheral Ready
SYSCTL_PREMAC           EQU 0x400FEA9C  ;// Ethernet MAC Peripheral Ready
SYSCTL_UNIQUEID0        EQU 0x400FEF20  ;// Unique ID 0
SYSCTL_UNIQUEID1        EQU 0x400FEF24  ;// Unique ID 1
SYSCTL_UNIQUEID2        EQU 0x400FEF28  ;// Unique ID 2
SYSCTL_UNIQUEID3        EQU 0x400FEF2C  ;// Unique ID 3
SYSCTL_CCMCGREQ         EQU 0x44030204  	;// Cryptographic Modules Clock
                                        ;// Gating Request
	END