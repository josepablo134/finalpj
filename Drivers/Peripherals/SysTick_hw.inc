;/*!
; *	DIRECCIONES DE LOS PERIFERICOS
;*/

;/*!
; *	32 BITS REGISTERS OFFSET FROM NVIC_BASE
;*/
SYSTICK_CTL					EQU		0x010  ; SysTick Control and Status Register
SYSTICK_RELOAD				EQU		0x014  ; SysTick Reload Register
SYSTICK_CURRENT				EQU		0x018  ; SysTick Current Count Register
	END
