;;//*****************************************************************************
;;//
;;// Controlador de sensor de temperatura DS18B20
;;//
;;//*****************************************************************************
;    /*!
;     * @brief  Inicializar los registros necesarios para activar el sensor.
;     *
;     *  @param  VOID
;     *
;     *  @return VOID
;     *
;    */
;    extern void DS18B20_init(void);
	EXTERN	DS18B20_init
;    /*!
;     * @brief  Transmitir un arreglo de bytes.
;     *
;     *  @param  uint8_t*	buffer
;	  *
;	  *	 @param  uint8_t	tamaño del buffer
;     *
;     *  @return VOID
;     *
;    */
;    extern void DS18B20_write(uint8_t* , uint8_t);
	EXTERN	DS18B20_write
;    /*!
;     * @brief  Recibir un arreglo de bytes.
;     *
;     *  @param  uint8_t*	buffer
;	  *
;	  *	 @param  uint8_t	tamaño del buffer
;     *
;     *  @return VOID
;     *
;    */
;    extern void DS18B20_read(uint8_t* , uint8_t);
	EXTERN	DS18B20_read
;    /*!
;     * @brief  Recibir un temperatura desde scratchpad y convertir a flotante.
;     *
;     *  @param  float*		buffer de resultado
;     *
;     *  @return VOID
;     *
;    */
;    extern void DS18B20_readTemp(float*);
	EXTERN	DS18B20_readTemp
	END