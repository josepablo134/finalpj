;;//*****************************************************************************
;;//
;;// Controlador de LCD2x16
;;//
;;//*****************************************************************************

;    /*!
;     * @brief  Inicializar los registros necesarios para activar el LCD.
;     *
;     *  @param  VOID
;     *
;     *  @return VOID
;     *
;    */
;    extern void LCD2x16_init(void);
    EXTERN  LCD2x16_init
;   /*!
;     * @brief  Escribir el comando correspondiente
;     *
;     *  @param  uint8_t	El comando que se debe transmitir
;
;     *  @return VOID
;     *
;    */
;    extern void LCD2x16_write(uint8_t);
    EXTERN  LCD2x16_write
;     /*!
;       * @brief  Escribir un texto en LCD
;       *
;       *  Esto implica llamar muchas veces a la funcion
;		*	putch hasta que el contador se agote.
;       *
;       *  @param  const uint8_t*   PTR TO CONST STRING
;		*  
;       *  @param  uint8_t          COUNTER
;       *
;       *  @return VOID
;       *
;      */
;     extern void LCD2x16_puts(const unsigned char* , unsigned char);
    EXTERN  LCD2x16_puts
;     /*!
;       * @brief  Escribir un caracter en LCD
;       *
;       *  Esto implica utilizar la funcion write
;       *
;       *  @param  uint8_t	    Character
;       *
;       *  @return VOID
;       *
;      */
;     extern void LCD2x16_putch(unsigned char);
    EXTERN  LCD2x16_putch
;     /*!
;       * @brief  Escribir un numero decimal de 8 bits en pantalla
;       *
;       *  Esto implica convertir el numero en caracter y utilizar putch
;       *
;       *  @param  uint8_t	    digit
;       *
;       *  @return VOID
;       *
;      */
;     extern void LCD2x16_putInt(unsigned char);
    EXTERN  LCD2x16_putInt
	END