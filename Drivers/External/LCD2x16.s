;;//*****************************************************************************
;;//
;;// Implementacion del controlador LCD2x16
;;//
;;//*****************************************************************************
    EXPORT  LCD2x16_init
    EXPORT  LCD2x16_write
    EXPORT  LCD2x16_puts
    EXPORT  LCD2x16_putch
    EXPORT  LCD2x16_putInt
	
	AREA    |.text|, CODE, READONLY , ALIGN=2
	THUMB
LCD2x16_init
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
LCD2x16_write
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
LCD2x16_puts
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
LCD2x16_putch
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
LCD2x16_putInt
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
	END