;;//*****************************************************************************
;;//
;;// Implementacion del controlador DS18B20
;;//
;;//*****************************************************************************
	EXPORT	DS18B20_init
	EXPORT	DS18B20_write
	EXPORT	DS18B20_read
	EXPORT	DS18B20_readTemp

	AREA    |.text|, CODE, READONLY , ALIGN=2
	THUMB
DS18B20_init
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
DS18B20_write
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
DS18B20_read
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
DS18B20_readTemp
	PUSH	{R0-R7,LR};GUARDAR CONTEXTO
	; Insertar codigo aqui
	POP		{R0-R7,PC};RETURN
	END