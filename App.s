	;=======================================
	;	Drivers del sistema
	;=======================================
	INCLUDE Drivers/Peripherals/Peripherals.inc		;	Lista de direcciones de todos los perifericos
	INCLUDE Drivers/Peripherals/GPIO_hw.inc			;	Lista de registros del periferico GPIO generico
	INCLUDE Drivers/Peripherals/SysCtl_hw.inc		;	Lista de registros del periferico SysCtl
	INCLUDE	Drivers/Peripherals/SysTick.inc			;	Funciones del SysTick
	INCLUDE Drivers/Peripherals/GPIO_driver.inc		;	Funciones de puertos GPIO
	;========================================
	;	Drivers de perifericos externos
	;========================================
	INCLUDE	Drivers/External/DS18B20.inc
	INCLUDE	Drivers/External/LCD2x16.inc
	
;SYSTICK_10MS_PRECHARGUE		EQU		3999999;
SYSTICK_1MS_PRECHARGUE		EQU		3999;
USER_LED					EQU		BIT0

BTN_MASK					EQU		0x03
BTN_LEFT					EQU		BIT1
BTN_RIGHT					EQU		BIT0
	
    AREA    |.text|, CODE, READONLY, ALIGN=2
	THUMB
	EXPORT	__main
__main	PROC
;===========================
;	CONIGURAR EL SYSTICK A 1MS
;===========================
	BL		SysTick_init
	MOV32	R0,#SYSTICK_1MS_PRECHARGUE
	BL		SysTick_write
	BL		SysTick_start
;==============================
;	CONFIGURAR PUERTOS GPIO
;==============================
	LDR		R0,=SYSCTL_RCGCGPIO
	MOV		R1,#(0x7FFF)	;CONFIGURAR TODOS LOS PUERTOS
	LDR		R2,[R0]
	ORR		R2,R1
	STR		R2,[R0]
	LDR		R0,=SYSCTL_PRGPIO
WAIT_FOR_PERIPH
	LDR		R2,[R0]
	AND		R2,R1
	CMP		R2,R1
	BNE		WAIT_FOR_PERIPH
	;
	;	Configurar puerto J como entrada
	;		usando el driver
	ldr		r0,=GPIO_PORTJ_BASE
	mov		r1,#0x00
	BL		GPIO_init
	;
	;	Configurar Pull-Up Resistors
	;		usando el driver
	ldr		R0,=GPIO_PORTJ_BASE
	mov		r1,#0xFF
	STRB	R1,[R0,#GPIO_O_PUR]

	;Configurar puerto N como salida
	ldr		r0,=GPIO_PORTN_BASE
	mov		r1,#0xFF
	BL		GPIO_init
;================================
;	CONFIGURAR SENSOR DS18B20
;================================
	BL		DS18B20_init
;================================
;	CONFIGURAR LCD
;================================
	BL		LCD2x16_init
Loop
	;	WAIT 100 MS
;	MOV32	R0,#250
;	BL		SysTick_delay
	LDR		R0,=GPIO_PORTJ_BASE		; Leer cualquier boton y encender el led
	MOV		R1,#BTN_MASK
	BL		GPIO_read
	CMP		R0,#BTN_MASK
	BEQ		TURN_OFF
	BNE		TURN_ON
TURN_OFF
	LDR		R0,=GPIO_PORTN_BASE
	MOV		R1,#USER_LED
	MOV		R2,#0x00
	BL		GPIO_write
	B		Loop
TURN_ON
	LDR		R0,=GPIO_PORTN_BASE
	MOV		R1,#USER_LED
	MOV		R2,#USER_LED
	BL		GPIO_write
	B		Loop
	ENDP; MAKE SURE THE END OF THIS SECTION IS ALIGN
	ALIGN
	END